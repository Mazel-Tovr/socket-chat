package com.epam.chat;

import com.epam.chat.businesslayer.ClientService;
import com.epam.chat.model.ClassConfig;
import com.epam.chat.model.User;

import java.io.IOException;
import java.util.Scanner;

public class NewClient
{
    public static void main(String[] args) throws IOException {
        ClassConfig config = ClassConfig.getInstance();;
        System.out.print("Введите nickname: ");
        Scanner in = new Scanner(System.in);

        while (true)
        {
            String s = in.nextLine();
            if(!s.isEmpty()) {
                new ClientService(config.getSERVER_ADDRESS(), config.getPORT(), new User(s));
                break;
            }
        }

    }
}
