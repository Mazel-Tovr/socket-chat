package com.epam.chat.model;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ClassConfig
{
    private String SERVER_ADDRESS;
    private int PORT;
    private String FILE_PATH;

    private static ClassConfig instance;

    public static ClassConfig getInstance()
    {
        if(instance != null) return instance;

        synchronized (ClassConfig.class)
        {
            if(instance == null)
            {
                instance = new ClassConfig();
            }
            return instance;
        }

    }

    private ClassConfig()
    {
        try {

            Properties properties = new Properties();
            String propFileName = "config.properties";

            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
            if(inputStream != null) {
                properties.load(inputStream);
                SERVER_ADDRESS = properties.getProperty("SERVER_ADDRESS");
                PORT = Integer.parseInt(properties.getProperty("SERVER_PORT"));
                FILE_PATH = properties.getProperty("FILE_PATH");
                inputStream.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String getSERVER_ADDRESS() {
        return SERVER_ADDRESS;
    }

    public int getPORT() {
        return PORT;
    }

    public String getFILE_PATH() {
        return FILE_PATH;
    }
}
