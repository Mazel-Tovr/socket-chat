package com.epam.chat.businesslayer;

import com.epam.chat.model.User;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;

public class ClientService
{
    private Socket socket;

    private BufferedReader in; // поток чтения из сокета
    private BufferedWriter out; // поток чтения в сокет
    private BufferedReader inputUser; // поток чтения с консоли
    private boolean threadWorking = true;

    // Инициализация логера
    private static final Logger logger = Logger.getRootLogger();

    private User user;

    public ClientService(String address, int port, User user)
    {

        this.user = user;

        try {
            this.socket = new Socket(address, port);
        } catch (IOException e) {
           logger.error("Не удается подключиться к серверу.");
            return;
        }

        try {
            inputUser = new BufferedReader(new InputStreamReader(System.in));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            startWriteAndReadTreads();
            Thread.sleep(100);
            System.out.println("Write \"!exit\" to exit from chat");
        } catch (IOException | InterruptedException ex)
        {
            ClientService.this.closeService();
        }
    }

    /**
     * Запуск потока на отправку сообщений и прием сообщений
     */
    private void startWriteAndReadTreads()
    {
        //Чтение
        new Thread(()->{
            String str;
            logger.debug("\"ReadTread\" started");
            try {
                while (threadWorking)
                {
                    str = in.readLine(); // ждем сообщения с сервера

                    System.out.println(str); // Вывводим  на консоль
                }
            } catch (IOException e) {
                ClientService.this.closeService();
            }

        }).start();

        //Запись
        new Thread(()->{
            while (threadWorking) {

                logger.debug("\"WritheTread\" started");
                String userWord;
                try {
                    userWord = inputUser.readLine(); // сообщения с консоли
                    if(userWord.equals("!exit"))
                    {
                        out.write(userWord);
                        out.flush();
                        this.closeService();
                        break;
                    }
                    if(!userWord.isEmpty()) {
                        out.write("<" + user.getNickName() + "> " + userWord + "\n"); // отправляем на сервер
                        out.flush(); // Выталкиваем сообщение из пула
                    }
                } catch (IOException e) {
                    ClientService.this.closeService();
                }

            }
        }).start();
    }


    /**
     * Закрываем сокет и все потоки
     */
    private void closeService()
    {
        try {
            if (!socket.isClosed())
            {
                threadWorking = false;
                socket.close();
                in.close();
                out.close();
                logger.debug("Read and Write Threads are stopped ");
            }
        } catch (IOException ignored) {
        }
    }

}

