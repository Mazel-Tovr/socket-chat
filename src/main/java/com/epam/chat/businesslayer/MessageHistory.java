package com.epam.chat.businesslayer;

import com.epam.chat.model.ClassConfig;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс для работы с историей сообщений
 */
public class MessageHistory {


    private ClassConfig config = ClassConfig.getInstance();

    /**
     * Путь к файлу
     */
    private final String FILE_PATH = config.getFILE_PATH();//"story.txt";


    /**
     * Метод для получения истории сообщений
     * @return лист с историей
     */
    public List<String> getMessageHistoryFromFile()
    {
        List<String> messageHistory = new ArrayList<>();
        try {
            Files.lines(Paths.get(FILE_PATH), StandardCharsets.UTF_8).forEach(messageHistory::add);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return messageHistory;
    }

    /**
     * Запись в файл истории собщений
     * @param story - лист с историей
     */
    public void writeMessageHistoryIntoFile(List<String> story) {

        try (FileWriter writer = new FileWriter(FILE_PATH))
        {
            for (String s : story)
            {
                writer.write(s+"\n");
            }
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}