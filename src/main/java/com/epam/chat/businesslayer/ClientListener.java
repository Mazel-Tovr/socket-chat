package com.epam.chat.businesslayer;


import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;


/**
 *  класс, обрабатывающий подключение клиента к серверу.
 */
public class ClientListener implements Runnable
{


    private Server server;

    // исходящее сообщение
    //private BufferedWriter outMessage;
    private PrintWriter outMessage;
    // входящее собщение
    private Scanner inMessage;

    // клиентский сокет
    private Socket clientSocket = null;



    /**
     *
     * @param clientSocket - сокет клиента
     * @param server - сокет сервера
     */
    public ClientListener(Socket clientSocket, Server server, List<String> messageHistory) {

       try {

           this.clientSocket = clientSocket;
           this.server = server;
           this.outMessage = new PrintWriter(clientSocket.getOutputStream());
           this.inMessage = new Scanner(clientSocket.getInputStream());
           messageHistory.forEach(this::sendMsg);

       } catch (IOException e) {
           e.printStackTrace();
       }
    }

    /**
     * Поток который служит для рассылки, полученного от одного юзера сообщения,  всем юзерам (через сервер)
     */
    @Override
    public void run() {

        try {

            server.sendMessageToAllClients("<"+server.getSERVER().getNickName()+"> "+"Новый участник вошёл в чат!");

            while (true)
            {
                // Если от клиента пришло сообщение
                if (inMessage.hasNext())
                {
                    String clientMessage = inMessage.nextLine();
                    if(clientMessage.equals("!exit"))
                    {
                       break;
                    }
                    server.sendMessageToAllClients(clientMessage);
                }
            }

        }
        finally {
            this.close();
        }

    }

    /**
     * Отправляет сообщение клиенту
     * @param msg - сообщение
     */
    public void sendMsg(String msg)
    {
        try {
            outMessage.println(msg);
            outMessage.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * При выходе клиент выходит из чата
     */
    public void close()
    {

        try {
            //Закрываем потоки и сокет
            outMessage.close();
            inMessage.close();
            clientSocket.close();
            // удаляем клиента из списка
            server.removeClient(this);
            server.sendMessageToAllClients("<"+server.getSERVER().getNickName()+"> "+"Клиент покинул чат");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
