package com.epam.chat.businesslayer;

import com.epam.chat.model.ClassConfig;
import com.epam.chat.model.User;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Класс, описывающий логику работы сервера.
 */
public class Server
{
    private ClassConfig config = ClassConfig.getInstance();

    private final int PORT = config.getPORT();

    private final User SERVER = new User("Server");

    private List<ClientListener> clients = new ArrayList<>();

    private List<String> historyList;

    //Сокет по которому будет осуществлятся подключение к серверу
    private Socket connectionSocket = null;

    // серверный сокет
    private ServerSocket serverSocket = null;

    private boolean isServerAlive = false;

    //Класс для работы с хранилещем сообщений
    private MessageHistory messageHistory = new MessageHistory();

    // Инициализация логера
    private static final Logger logger = Logger.getRootLogger();

    public User getSERVER() { return SERVER; }

    public boolean isServerAlive() { return isServerAlive; }

    public Server() {

        try
        {
            serverSocket = new ServerSocket(PORT);
           // System.out.println("Server start to work");
            logger.info("Server start to work");
            isServerAlive = true;
            //System.out.println("Write \"!stop\" to off server");
            logger.info("Write \"!stop\" to off server");
            historyList = messageHistory.getMessageHistoryFromFile();
            stopServerListener();
            while (isServerAlive)
            {
                connectionSocket = serverSocket.accept();
                ClientListener listener = new ClientListener(connectionSocket,this, historyList);
                //Добавляем слушатель в список
                clients.add(listener);
                logger.debug("new user connected");
                //Стартуем новый поток для слушателя клиента
                new Thread(listener).start();
            }

        } catch (IOException e ) {
            logger.error(e.getMessage());
        } finally
        {
            try {
                closeServer();
                logger.info("Server stopped");
                //System.out.println("Server stopped");
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
    }
    // отправляем сообщение всем клиентам
    public void sendMessageToAllClients(String msg) {
        logger.info(msg);
        //System.out.println(msg);
        if(historyList.size() >= 5) historyList.remove(0);
        historyList.add(msg);

        for (ClientListener o : clients)
        {
            o.sendMsg(msg);
        }

    }

    // удаляем клиента из коллекции при выходе из чата
    public void removeClient(ClientListener client) {
        clients.remove(client);
    }

    private void closeServer() throws IOException
    {

        messageHistory.writeMessageHistoryIntoFile(historyList);
        if(connectionSocket != null) connectionSocket.close();
        serverSocket.close();
        isServerAlive = false;
    }

    private void stopServerListener()
    {
        new Thread(new Runnable() {
            @Override
            public void run()
            {
                logger.debug("started new thread \"stopServerListener\"");
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    logger.error(e.getMessage());
                }
                Scanner in = new Scanner(System.in);
                while (isServerAlive)
                {
                    if(in.nextLine().equals("!stop"))
                    {
                    try {
                        closeServer();
                    } catch (IOException e) {
                        logger.error(e.getMessage());
                    }
                     }
                }
                logger.debug("\"stopServerListener\" thread stopped");
            }
        }).start();
    }


}
