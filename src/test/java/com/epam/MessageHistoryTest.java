package com.epam;

import com.epam.chat.businesslayer.MessageHistory;
import com.epam.chat.model.ClassConfig;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MessageHistoryTest
{
    MessageHistory messageHistory = new MessageHistory();

    @Test
    public void readWriteTestText()
    {

        List<String> actualMessage = new ArrayList<>();
        actualMessage.add("message 1");
        actualMessage.add("message 2");
        actualMessage.add("message 3");
        actualMessage.add("message 4");
        actualMessage.add("message 5");

        messageHistory.writeMessageHistoryIntoFile(actualMessage);
        List<String> expected = messageHistory.getMessageHistoryFromFile();
        assertEquals(actualMessage,expected);

    }

    @Test
    public void readWriteTestSymbols()
    {
        List<String> actualMessage = new ArrayList<>();
        actualMessage.add("@#2131414");
        actualMessage.add("hfgh45324#@$#!$!@#FDGVXVC");
        actualMessage.add("sfssfdsfsdfsdfssffsfs");
        actualMessage.add("$@!@#@!CVDSFSVSSDF:(#!@");
        actualMessage.add("(#!#!#FDFST$!#@!@T@#@)");

        messageHistory.writeMessageHistoryIntoFile(actualMessage);
        List<String> expected = messageHistory.getMessageHistoryFromFile();
        assertEquals(actualMessage,expected);
    }
    @Test
    public void readWriteTestNumbers()
    {
        List<String> actualMessage = new ArrayList<>();
        actualMessage.add("31232132153546769876986");
        actualMessage.add("42342342424");
        actualMessage.add("423424234");
        actualMessage.add("43636346363");
        actualMessage.add("4337876976346");

        messageHistory.writeMessageHistoryIntoFile(actualMessage);
        List<String> expected = messageHistory.getMessageHistoryFromFile();
        assertEquals(actualMessage,expected);
    }
    @Test
    public void readWriteTestMixed()
    {
        List<String> actualMessage = new ArrayList<>();
        actualMessage.add("РПавпвпвлпо4оо41241оовп");
        actualMessage.add("рпар укеук укеук452 32пва");
        actualMessage.add("    ");
        actualMessage.add("42тимиекуи646%№;!№");
        actualMessage.add(";\" + 4324242");

        messageHistory.writeMessageHistoryIntoFile(actualMessage);
        List<String> expected = messageHistory.getMessageHistoryFromFile();
        assertEquals(actualMessage,expected);
    }
}
