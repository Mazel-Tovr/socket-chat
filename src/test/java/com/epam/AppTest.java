package com.epam;

import static org.junit.Assert.assertTrue;

import com.epam.chat.businesslayer.Server;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses(value = {MessageHistoryTest.class})
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
    }
}
